package py.com.ecommerce.productapi.beans;

import java.util.List;

/**
 * Created by jcaceresf on 9/24/20
 */
public class CompositeProduct {

    private Product product;
    private List<Review> reviews;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }
}
