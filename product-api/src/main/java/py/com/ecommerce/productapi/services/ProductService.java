package py.com.ecommerce.productapi.services;

import py.com.ecommerce.productapi.beans.CompositeProduct;
import py.com.ecommerce.productapi.beans.EcommerceException;
import py.com.ecommerce.productapi.beans.Product;
import py.com.ecommerce.productapi.beans.ProductFilter;

import java.util.List;

/**
 * Created by jcaceresf on 9/24/20
 */
public interface ProductService {

    void saveProduct(Product product) throws EcommerceException;

    Product getProductByCode(String code) throws EcommerceException;

    CompositeProduct getCompositeProduct(String code) throws EcommerceException;

    List<Product> getProductList() throws EcommerceException;

    void checkProductStock(String code, int stock) throws EcommerceException;

    List<Product> getProductByFilters(ProductFilter productFilter) throws EcommerceException;

}
