package py.com.ecommerce.productapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import py.com.ecommerce.productapi.beans.EcommerceException;
import py.com.ecommerce.productapi.beans.Product;
import py.com.ecommerce.productapi.services.ProductService;

import java.util.List;

/**
 * Created by jcaceresf on 9/24/20
 */
@RestController
@RequestMapping(value = "/products")
public class ProductAPI {

    @Autowired
    private ProductService productService;


    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Product>> getProducts() throws EcommerceException {

        return ResponseEntity.ok(productService.getProductList());

    }

}
