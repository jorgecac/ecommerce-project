package py.com.ecommerce.productapi.services;

import py.com.ecommerce.productapi.beans.Brand;
import py.com.ecommerce.productapi.beans.EcommerceException;

import java.util.List;

/**
 * Created by jcaceresf on 9/24/20
 */
public interface BrandService {

    void saveBrand(Brand brand) throws EcommerceException;

    List<Brand> getBrands() throws EcommerceException;

    Brand getBrand(String code) throws EcommerceException;

}
