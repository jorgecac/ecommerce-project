package py.com.ecommerce.productapi.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by jcaceresf on 9/24/20
 */
public class Brand {

    @JsonIgnore
    private Long brandIID;

    private String code;
    private String name;
    private String description;

    public Long getBrandIID() {
        return brandIID;
    }

    public void setBrandIID(Long brandIID) {
        this.brandIID = brandIID;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
