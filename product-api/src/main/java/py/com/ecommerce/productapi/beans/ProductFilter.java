package py.com.ecommerce.productapi.beans;

import java.math.BigDecimal;

/**
 * Created by jcaceresf on 9/24/20
 */
public class ProductFilter {

    private String categoryCode;
    private String brandCode;
    private BigDecimal minPrice;
    private BigDecimal maxPrice;
    private Integer mediaReviews;

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Integer getMediaReviews() {
        return mediaReviews;
    }

    public void setMediaReviews(Integer mediaReviews) {
        this.mediaReviews = mediaReviews;
    }
}
