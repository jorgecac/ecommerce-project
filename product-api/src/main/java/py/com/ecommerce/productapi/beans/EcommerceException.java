package py.com.ecommerce.productapi.beans;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by jcaceresf on 9/24/20
 */
public class EcommerceException extends Exception {

    private String code;
    private LocalDateTime tstamp;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalDateTime getTstamp() {
        return tstamp;
    }

    public void setTstamp(LocalDateTime tstamp) {
        this.tstamp = tstamp;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
