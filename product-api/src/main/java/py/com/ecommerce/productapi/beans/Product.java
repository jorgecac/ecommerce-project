package py.com.ecommerce.productapi.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by jcaceresf on 9/24/20
 */
public class Product {

    @JsonIgnore
    private Long productIID;

    private Category category;
    private Brand brand;

    private String code;
    private String description;
    private String specifications;

    private BigDecimal discountPrice;
    private BigDecimal price;
    private int stock;
    private Map<String, Object> additionalData;

    public Long getProductIID() {
        return productIID;
    }

    public void setProductIID(Long productIID) {
        this.productIID = productIID;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSpecifications() {
        return specifications;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }

    public BigDecimal getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(BigDecimal discountPrice) {
        this.discountPrice = discountPrice;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public Map<String, Object> getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(Map<String, Object> additionalData) {
        this.additionalData = additionalData;
    }

    public Product(Long productIID, Category category, Brand brand, String code, String description, String specifications, BigDecimal discountPrice, BigDecimal price, int stock, Map<String, Object> additionalData) {
        this.productIID = productIID;
        this.category = category;
        this.brand = brand;
        this.code = code;
        this.description = description;
        this.specifications = specifications;
        this.discountPrice = discountPrice;
        this.price = price;
        this.stock = stock;
        this.additionalData = additionalData;
    }

    public Product() {
    }
}
