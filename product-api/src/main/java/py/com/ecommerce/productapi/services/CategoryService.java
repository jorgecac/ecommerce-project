package py.com.ecommerce.productapi.services;

import py.com.ecommerce.productapi.beans.Category;
import py.com.ecommerce.productapi.beans.EcommerceException;

import java.util.List;

/**
 * Created by jcaceresf on 9/24/20
 */
public interface CategoryService {

    void saveCategory(Category category) throws EcommerceException;

    List<Category> getCategories() throws EcommerceException;

    Category getCategory(String code) throws EcommerceException;

}
