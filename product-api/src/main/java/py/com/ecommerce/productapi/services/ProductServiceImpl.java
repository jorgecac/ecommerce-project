package py.com.ecommerce.productapi.services;

import org.springframework.stereotype.Service;
import py.com.ecommerce.productapi.beans.CompositeProduct;
import py.com.ecommerce.productapi.beans.EcommerceException;
import py.com.ecommerce.productapi.beans.Product;
import py.com.ecommerce.productapi.beans.ProductFilter;

import java.util.Arrays;
import java.util.List;

/**
 * Created by jcaceresf on 9/24/20
 */
@Service
public class ProductServiceImpl implements ProductService {
    @Override
    public void saveProduct(Product product) throws EcommerceException {

    }

    @Override
    public Product getProductByCode(String code) throws EcommerceException {
        return null;
    }

    @Override
    public CompositeProduct getCompositeProduct(String code) throws EcommerceException {
        return null;
    }

    @Override
    public List<Product> getProductList() throws EcommerceException {
        return Arrays.asList(new Product(null, null, null, null, null, null, null, null, 1, null));
    }

    @Override
    public void checkProductStock(String code, int stock) throws EcommerceException {

    }

    @Override
    public List<Product> getProductByFilters(ProductFilter productFilter) throws EcommerceException {
        return null;
    }
}
