package py.com.ecommerce.productapi.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by jcaceresf on 9/24/20
 */
public class Category {

    @JsonIgnore
    private Long categoryIID;

    private String code;
    private String description;

    public Long getCategoryIID() {
        return categoryIID;
    }

    public void setCategoryIID(Long categoryIID) {
        this.categoryIID = categoryIID;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
